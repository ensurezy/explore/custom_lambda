#!/bin/bash
echo 'export PS1="[custom_lambda_nested container \u@\h: \w ] $ "' >> ~/.bashrc
echo 'alias aa="cd /src/tests"' >> ~/.bashrc
echo 'alias forest="ps -ef --forest"' >> ~/.bashrc
echo 'alias ll="ls -lnh"' >> ~/.bashrc

rm -rf /dev/dri
mkdir -p /output

##disable ipv6
sysctl -p

tail -f /dev/null

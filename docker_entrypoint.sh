#!/bin/bash

rm -rf /dev/dri

echo 'export PS1="[custom_lambda container \u@\h: \w ] $ "' >> ~/.bashrc

echo 'alias pps="podman ps -a"' >> ~/.bashrc
echo 'alias pils="podman image ls"' >> ~/.bashrc
echo 'alias pstop="podman stop -a"' >> ~/.bashrc
echo 'alias pcp="podman container prune -f"' >> ~/.bashrc
echo 'alias pprune="podman system prune -af"' >> ~/.bashrc
echo 'alias forest="ps -ef --forest"' >> ~/.bashrc
echo 'alias ll="ls -lnh"' >> ~/.bashrc

mkdir -p /criu

podman network create custom_lambda --ignore

podman build --platform linux/amd64 --network custom_lambda -f src/Dockerfile -t custom_lambda_nested:latest .

podman system service --time=0 unix:///var/run/podman.sock
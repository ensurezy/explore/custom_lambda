import errno
import fcntl
import logging
import os
import shutil


def makedirs(dir_path):
    try:
        os.makedirs(dir_path)
    except OSError as er:
        if er.errno == errno.EEXIST and os.path.isdir(dir_path):
            pass
        else:
            raise


def set_cloexec(sk):
    flags = fcntl.fcntl(sk, fcntl.F_GETFD)
    fcntl.fcntl(sk, fcntl.F_SETFD, flags | fcntl.FD_CLOEXEC)


class CruiOpenDirectory(object):
    def __init__(self, path):
        self._dirname = path
        self._dirfd = os.open(path, os.O_DIRECTORY)
        set_cloexec(self)

    def close(self):
        os.close(self._dirfd)
        os._dirname = None
        os._dirfd = -1

    def name(self):
        return self._dirname

    def fileno(self):
        return self._dirfd


class CriuImagesPath(object):
    def __init__(self, img_path, auto_dedup=True, tcp_skip_in_flight=True, keep_on_close=True):
        self.img_path = img_path
        self.auto_dedup = auto_dedup
        self.tcp_skip_in_flight = tcp_skip_in_flight

        self._current_dir = img_path
        self._keep_on_close = keep_on_close

        makedirs(self.img_path)
        self._current_dir = CruiOpenDirectory(self.img_path)

    def close(self):
        if not self._current_dir:
            return

        self._current_dir.close()

        if not self._keep_on_close:
            logging.info(f"Removing criu images in {self._current_dir.name()}")
            shutil.rmtree(self._current_dir.name())
        else:
            logging.info("Criu images are kept in %s", self._current_dir.name())

    def image_dir_fd(self):
        return self._current_dir.fileno()

    def image_dir(self):
        return self._current_dir.name()

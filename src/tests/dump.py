import json
import logging
import os.path
import shlex
import shutil
import signal
import subprocess
import sys
from subprocess import Popen

from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.chrome.webdriver import WebDriver as ChromeDriver
from selenium.webdriver.common.by import By


sys.path.append("/src")

from chromedriver_utils import (
    CustomChromeDriver,
    CustomChromeService,
    get_chrome_options,
    get_display_and_xvnc_ports,
    wait_for_process_to_finish,
    USER_AGENT_OVERRIDE,
    CHROMEDRIVER_BINARY_PATH,
    WINDOW_WIDTH,
    WINDOW_HEIGHT,
)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
ch = logging.StreamHandler()
fh = logging.FileHandler("/output/test.log")
ch.setLevel(logging.DEBUG)
fh.setLevel(logging.DEBUG)
logger.addHandler(ch)
logger.addHandler(fh)


if os.path.exists("/dev/dri"):
    try:
        shutil.rmtree("/dev/dri", ignore_errors=True)
    except Exception as e:
        logger.exception(e)

chrome_service_kwargs = dict()
chrome_service_kwargs["log_output"] = "/output/chromedriver.log"
chrome_service_kwargs["service_args"] = ["--verbose"]

selenium_chrome_options = get_chrome_options()

DISPLAY, XVNC_PORT = get_display_and_xvnc_ports()

if DISPLAY:
    if XVNC_PORT:
        chrome_service_kwargs[
            "executable_path"
        ] = f"/src/xvnc_server.sh {CHROMEDRIVER_BINARY_PATH}"
    else:
        chrome_service_kwargs[
            "executable_path"
        ] = f"/src/xvfb_server.sh {CHROMEDRIVER_BINARY_PATH}"

    recording_file_path = "/output/recording1.mkv"
    ffmpeg_stream = f'ffmpeg -video_size {WINDOW_WIDTH}x{WINDOW_HEIGHT} -framerate 25 -f x11grab -i {DISPLAY}.0 "{recording_file_path}"'
    args = shlex.split(ffmpeg_stream)
    recording_process = Popen(
        args,
        preexec_fn=os.setsid,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    driver = CustomChromeDriver(
        options=selenium_chrome_options,
        service=CustomChromeService(**chrome_service_kwargs),
    )
else:
    selenium_chrome_options.add_argument("--headless=new")

    chrome_service_kwargs["executable_path"] = CHROMEDRIVER_BINARY_PATH

    driver = ChromeDriver(
        options=selenium_chrome_options, service=ChromeService(**chrome_service_kwargs)
    )
    recording_process = None


# override browser properties to prevent bot detection
driver.execute_script(
    "Object.defineProperty(navigator, 'webdriver', {get: () => undefined})"
)
driver.execute_script(
    "Object.defineProperty(navigator, 'plugins', {get: () => [1, 2, 3, 4, 5]})"
)
driver.execute_script(
    'Object.defineProperty(navigator, "language", {get: () => "en-GB"})'
)

driver.execute_script(
    'Object.defineProperty(navigator, "languages", {get: () => ["en-GB","en-US","en"],})'
)
driver.execute_cdp_cmd("Emulation.setUserAgentOverride", USER_AGENT_OVERRIDE)

driver_executor_url = driver.service.service_url
driver_session_id = driver.session_id
chrome_debugger_address = driver.caps.get("goog:chromeOptions").get("debuggerAddress")
chrome_user_data_dir = driver.caps.get("chrome").get("userDataDir")

logger.info(f"driver url: {driver_executor_url}")
logger.info(f"driver session_id: {driver_session_id}")
logger.info(f"chrome debugger_address: {chrome_debugger_address}")
logger.info(f"chrome user_data_location: {chrome_user_data_dir}")

driver.get("https://google.com")
e = driver.find_element(By.TAG_NAME, "body")
logger.info(f"first session page: {e.text} \n\n\n")

driver.get_screenshot_as_file("/output/capture1.png")

if recording_process:
    os.killpg(os.getpgid(recording_process.pid), signal.SIGINT)
    wait_for_process_to_finish(recording_process)

chromedriver_process_dump_details = {
    "dump_count": 1,
    "chrome_debugger_address": chrome_debugger_address,
    "chrome_user_data_dir": chrome_user_data_dir,
}

logger.info(f"dumping chrome process details: {chromedriver_process_dump_details}")

with open("/output/chromedriver_process_dump_details.json", "w") as f:
    json.dump(chromedriver_process_dump_details, f)

driver.service.stop()

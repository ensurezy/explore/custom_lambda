import json
import logging
import os
import shlex
import shutil
import signal
import subprocess
import sys
from subprocess import Popen

from selenium.webdriver import ChromeOptions
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.chrome.webdriver import WebDriver as ChromeDriver
from selenium.webdriver.common.by import By

sys.path.append("/src")

from chromedriver_utils import (
    CustomChromeDriver,
    CustomChromeService,
    get_display_and_xvnc_ports,
    wait_for_process_to_finish,
    CHROMEDRIVER_BINARY_PATH,
)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
ch = logging.StreamHandler()
fh = logging.FileHandler("/output/test.log")
ch.setLevel(logging.DEBUG)
fh.setLevel(logging.DEBUG)
logger.addHandler(ch)
logger.addHandler(fh)


if os.path.exists("/dev/dri"):
    try:
        shutil.rmtree("/dev/dri", ignore_errors=True)
    except Exception as e:
        logger.exception(e)


urls = {
    1: "https://example.com",
    2: "https://github.com",
    3: "https://gitlab.com",
    4: "https://facebook.com",
}


if os.path.exists("/output/chromedriver_process_dump_details.json"):
    with open("/output/chromedriver_process_dump_details.json", "r") as f:
        chromedriver_process_dump_details = json.load(f)

    selenium_chrome_options = ChromeOptions()

    DISPLAY, XVNC_PORT = get_display_and_xvnc_ports()

    chrome_service_kwargs = dict()
    chrome_service_kwargs["log_output"] = "/output/chromedriver.log"

    dump_count = chromedriver_process_dump_details.get("dump_count", 0)
    chrome_debugger_address = chromedriver_process_dump_details.get(
        "chrome_debugger_address"
    )
    chrome_user_data_dir = chromedriver_process_dump_details.get("chrome_user_data_dir")

    selenium_chrome_options.debugger_address = chrome_debugger_address

    WINDOW_WIDTH = 2560
    WINDOW_HEIGHT = 1600

    if DISPLAY:
        if XVNC_PORT:
            chrome_service_kwargs[
                "executable_path"
            ] = f"/src/newns /src/xvnc_server.sh {CHROMEDRIVER_BINARY_PATH}"
        else:
            chrome_service_kwargs[
                "executable_path"
            ] = f"/src/newns /src/xvfb_server.sh {CHROMEDRIVER_BINARY_PATH}"

        recording_file_path = f"/output/recording{dump_count}.mkv"
        ffmpeg_stream = f'ffmpeg -video_size {WINDOW_WIDTH}x{WINDOW_HEIGHT} -framerate 25 -f x11grab -i {DISPLAY}.0 "{recording_file_path}"'
        args = shlex.split(ffmpeg_stream)
        recording_process = Popen(
            args,
            preexec_fn=os.setsid,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        driver = CustomChromeDriver(
            options=selenium_chrome_options,
            service=CustomChromeService(**chrome_service_kwargs),
        )
    else:
        selenium_chrome_options.add_argument("--headless")

        chrome_service_kwargs["executable_path"] = CHROMEDRIVER_BINARY_PATH

        driver = ChromeDriver(
            options=selenium_chrome_options,
            service=ChromeService(**chrome_service_kwargs),
        )
        recording_process = None

    e = driver.find_element(By.TAG_NAME, "body")
    logger.info(f"last session page: {e.text} \n\n\n")
    driver.get(urls.get(dump_count, "https://example.com"))
    e = driver.find_element(By.TAG_NAME, "body")
    logger.info(f"new session page: {e.text} \n\n\n")

    if recording_process:
        os.killpg(os.getpgid(recording_process.pid), signal.SIGINT)
        wait_for_process_to_finish(recording_process)

    dump_count = dump_count + 1
    driver.get_screenshot_as_file(f"/output/capture{dump_count}.png")

    chromedriver_process_dump_details = {
        "dump_count": dump_count,
        "chrome_debugger_address": chrome_debugger_address,
        "chrome_user_data_dir": chrome_user_data_dir,
    }

    logger.info(f"dumping chrome process details: {chromedriver_process_dump_details}")

    with open("/output/chromedriver_process_dump_details.json", "w") as f:
        json.dump(chromedriver_process_dump_details, f)

    driver.service.stop()

import errno
import logging
import os
import subprocess
import time
from platform import system

import psutil
import undetected_chromedriver
from selenium.common import WebDriverException
from selenium.webdriver import ChromeOptions
from selenium.webdriver.chrome.service import Service as ChromeService, Service
from selenium.webdriver.chromium.remote_connection import ChromiumRemoteConnection
from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.options import ArgOptions

root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
ch = logging.StreamHandler()
fh = logging.FileHandler("/output/test.log")
ch.setLevel(logging.DEBUG)
fh.setLevel(logging.DEBUG)
logger.addHandler(ch)
logger.addHandler(fh)

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36"
ACCEPT_LANGUAGE = "en"
PLATFORM = "macOS"

USER_AGENT_OVERRIDE = {
    "userAgent": USER_AGENT,
    "platform": PLATFORM,
    "userAgentMetadata": {
        "brands": [
            {"brand": "Chromium", "version": "104"},
            {"brand": " Not A;Brand", "version": "99"},
            {"brand": "Google Chrome", "version": "104"},
        ],
        "platform": PLATFORM,
        "platformVersion": "10",
        "architecture": "x86",
        "model": "Monterey",
        "mobile": False,
    },
}

DISPLAY = os.getenv("DISPLAY")
XVNC_PORT = os.getenv("XVNC_PORT")


def wait_for_process_to_finish(process, count=0):
    """Check For the existence of a unix pid."""

    gone, alive = psutil.wait_procs([process], timeout=1)
    if alive:
        if count < 3:
            time.sleep(0.25)
            count = count + 1
            wait_for_process_to_finish(process, count)
        else:
            for p in alive:
                p.kill()


class CustomChromeService(ChromeService):
    def _start_process(self, path: str) -> None:
        """Creates a subprocess by executing the command provided.

        :param path: full command path to execute
        """
        cmd = path.split(" ")
        cmd.extend(self.command_line_args())
        close_file_descriptors = self.popen_kw.pop("close_fds", system() != "Windows")
        try:
            start_info = None
            if system() == "Windows":
                start_info = subprocess.STARTUPINFO()
                start_info.dwFlags = (
                    subprocess.CREATE_NEW_CONSOLE | subprocess.STARTF_USESHOWWINDOW
                )
                start_info.wShowWindow = subprocess.SW_HIDE

            self.process = subprocess.Popen(
                cmd,
                env=self.env,
                close_fds=close_file_descriptors,
                stdout=self.log_output,
                stderr=self.log_output,
                stdin=subprocess.PIPE,
                creationflags=self.creation_flags,
                startupinfo=start_info,
                **self.popen_kw,
            )
            logger.debug(
                "Started executable: `%s` in a child process with pid: %s using %s to output %s",
                self._path,
                self.process.pid,
                self.creation_flags,
                self.log_output,
            )
        except TypeError:
            raise
        except OSError as err:
            if err.errno == errno.EACCES:
                raise WebDriverException(
                    f"'{os.path.basename(self._path)}' executable may have wrong permissions."
                ) from err
            raise


class CustomChromiumDriver(ChromiumDriver):
    """Controls the WebDriver instance of ChromiumDriver and allows you to
    drive the browser."""

    def __init__(
        self,
        browser_name: str = None,
        vendor_prefix: str = None,
        options: ArgOptions = ArgOptions(),
        service: Service = None,
        keep_alive: bool = True,
    ) -> None:
        """Creates a new WebDriver instance of the ChromiumDriver. Starts the
        service and then creates new WebDriver instance of ChromiumDriver.

        :Args:
         - browser_name - Browser name used when matching capabilities.
         - vendor_prefix - Company prefix to apply to vendor-specific WebDriver extension commands.
         - options - this takes an instance of ChromiumOptions
         - service - Service object for handling the browser driver if you need to pass extra details
         - keep_alive - Whether to configure ChromiumRemoteConnection to use HTTP keep-alive.
        """
        self.service = service

        self.service.path = self.service._path
        self.service.start()

        executor = ChromiumRemoteConnection(
            remote_server_addr=self.service.service_url,
            browser_name=browser_name,
            vendor_prefix=vendor_prefix,
            keep_alive=keep_alive,
            ignore_proxy=options._ignore_local_proxy,
        )

        try:
            super(ChromiumDriver, self).__init__(
                command_executor=executor, options=options
            )
        except Exception:
            self.quit()
            raise

        self._is_remote = False


class CustomChromeDriver(CustomChromiumDriver):
    pass


CHROMEDRIVER_BINARY_PATH = "/custom_builds/chromedriver-linux64/chromedriver"

USE_UNDETECTED_CHROME_DRIVER = True
if USE_UNDETECTED_CHROME_DRIVER:
    CHROMEDRIVER_BINARY_PATH = CHROMEDRIVER_BINARY_PATH + "_undetected"
    patcher = undetected_chromedriver.Patcher(executable_path=CHROMEDRIVER_BINARY_PATH)

    undetected_chrome_driver_patched = patcher.is_binary_patched(
        patcher.executable_path
    )
    if not undetected_chrome_driver_patched:
        logger.info(
            f"Chrome-driver binary not patched for bot detection. Patching now : {patcher.executable_path}"
        )
        patcher.auto()


WINDOW_WIDTH = 2560
WINDOW_HEIGHT = 1600


def get_chrome_options(user_data_location="/output/chrome-user-data"):
    selenium_chrome_options = ChromeOptions()
    selenium_chrome_options.binary_location = "/custom_builds/chrome-linux64/chrome"
    selenium_chrome_options.add_argument(f"user-agent={USER_AGENT}")
    selenium_chrome_options.add_argument(
        f"--window-size={WINDOW_WIDTH},{WINDOW_HEIGHT}"
    )
    selenium_chrome_options.add_argument("--start-maximized")
    selenium_chrome_options.add_argument("--disable-infobars")
    selenium_chrome_options.add_argument("--hide-scrollbars")
    selenium_chrome_options.add_argument("--no-sandbox")
    selenium_chrome_options.add_argument("--no-zygote")
    selenium_chrome_options.add_argument("--disable-dev-shm-usage")
    selenium_chrome_options.add_argument("--single-process")
    selenium_chrome_options.add_argument("--disable-gpu")
    selenium_chrome_options.add_argument("--disable-gpu-sandbox")
    selenium_chrome_options.add_argument("--disable-setuid-sandbox")
    selenium_chrome_options.add_argument("--disable-crash-reporter")
    selenium_chrome_options.add_argument("--enable-logging")

    # ignore ssl verification when using selenium wire proxy with custom ssl certificate
    selenium_chrome_options.add_argument("--ignore-certificate-errors-spki-list")

    selenium_chrome_options.add_argument("--ignore-certificate-errors")
    selenium_chrome_options.add_argument("--ignore-ssl-errors")
    selenium_chrome_options.add_argument("--disable-ipv6")
    # disable automation detection
    selenium_chrome_options.add_argument(
        "--disable-blink-features=AutomationControlled"
    )
    selenium_chrome_options.add_argument("--disable-extensions")
    selenium_chrome_options.add_experimental_option(
        "excludeSwitches", ["enable-automation"]
    )
    selenium_chrome_options.add_experimental_option("useAutomationExtension", False)
    selenium_chrome_options.set_capability("pageLoadStrategy", "eager")
    selenium_chrome_options.set_capability("acceptInsecureCerts", True)

    selenium_chrome_options.add_argument("--disable-popup-blocking")

    if not os.path.exists(user_data_location):
        try:
            os.makedirs(user_data_location)
        except FileExistsError:
            pass

    selenium_chrome_options.add_argument("--user-data-dir=%s" % user_data_location)
    selenium_chrome_options.add_experimental_option("detach", True)

    return selenium_chrome_options


def get_display_and_xvnc_ports():
    return DISPLAY, XVNC_PORT

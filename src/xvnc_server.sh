#!/bin/bash
set -m
export LD_LIBRARY_PATH=/usr/local/lib64

mkdir -p /output/display

Xvfb $DISPLAY -screen 0 2560x1600x16 -fbdir /output/display/ &
xvfb_pid=$!

# Wait for Xvfb
MAX_ATTEMPTS=120 # About 60 seconds
COUNT=0
echo -n "Waiting for Xvfb to be ready..."
while ! xdpyinfo -display "${DISPLAY}" >/dev/null 2>&1; do
  echo -n "."
  sleep 0.50s
  COUNT=$(( COUNT + 1 ))
  if [ "${COUNT}" -ge "${MAX_ATTEMPTS}" ]; then
    echo "  Gave up waiting for X server on ${DISPLAY}"
    exit 1
  fi
done

x11vnc -forever -quiet -passwd "ensurezy" -rfbport $XVNC_PORT \
-rawfb map:/output/display/Xvfb_screen0@2560x1600x16 &

x11vnc_pid=$!

trap "kill $x11vnc_pid; kill $xvfb_pid; wait" EXIT

$@

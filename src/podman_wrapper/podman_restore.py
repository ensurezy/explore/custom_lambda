import random
from subprocess import getstatusoutput

from podman import PodmanClient


with PodmanClient(base_url="unix:///var/run/podman.sock") as client:
    custom_lambda_network = client.networks.get("custom_lambda")
    container_name = random.randint(0, 100000)
    restore_command = f"podman container restore --file-locks -i /dumps/check.tar.gz -n {container_name}"
    restore_output = getstatusoutput(restore_command)
    if restore_output[0] != 0:
        error = restore_output[1]
        print(error)
        raise Exception(f"Could not restore container: {error}")
    else:
        restore_output = str(restore_output[1])
        new_container = client.containers.get(restore_output)
        custom_lambda_network.connect(new_container)
        new_container.exec_run(
            privileged=True,
            tty=True,
            cmd="sysctl -p",
        )
        restore_results = new_container.exec_run(
            privileged=True, tty=True, workdir="/src/tests", cmd="python3 restore.py"
        )
        print(restore_results[1].decode("utf8", "ignore"))

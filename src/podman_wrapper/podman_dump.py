import time
from subprocess import getstatusoutput

from podman import PodmanClient

IMAGE_BUILD_TIMEOUT = 60 * 4
CONTAINER_START_TIMEOUT = 5
TCP_CONNECTION_DISCONNECT_TIMEOUT = 60


with PodmanClient(base_url="unix:///var/run/podman.sock") as client:
    base_image = None
    time_elapsed = 0
    while (time_elapsed < IMAGE_BUILD_TIMEOUT) and not base_image:
        for image in client.images.list():
            if any(["custom_lambda_nested:latest" in tag for tag in image.tags]):
                base_image = image
                break
        time_elapsed = time_elapsed + 1
        time.sleep(1)

    if not image:
        raise Exception("Image not ready in 5 min")

    default_network = client.networks.get("podman")
    custom_lambda_network = client.networks.get("custom_lambda")

    new_container = client.containers.run(
        base_image,
        privileged=True,
        detach=True,
        platform="linux/amd64",
        mounts=[
            {
                "type": "bind",
                "source": "/dumps",
                "target": "/dumps",
                "read_only": False,
            },
            {
                "type": "bind",
                "source": "/src",
                "target": "/src",
                "read_only": False,
            },
        ],
        networks={"custom_lambda": {"NetworkID": "custom_lambda"}},
    )
    time_elapsed = 0
    while (
        time_elapsed < CONTAINER_START_TIMEOUT
    ) and new_container.status != "running":
        time_elapsed = time_elapsed + 1
        time.sleep(1)

    if new_container.status != "running":
        raise Exception("Container did not start in 5 seconds")

    dump_results = new_container.exec_run(
        privileged=True, tty=True, workdir="/src/tests", cmd="python3 dump.py"
    )
    if dump_results[0] == 0:
        print(dump_results[1].decode("utf8", "ignore"))

        custom_lambda_network.disconnect(new_container)
        tcp_connection_closed = False
        time_elapsed = 0
        while time_elapsed < TCP_CONNECTION_DISCONNECT_TIMEOUT:
            lsof_response = new_container.exec_run(
                tty=True, cmd="lsof -i -sTCP:ESTABLISHED"
            )
            if lsof_response[1]:
                time_elapsed = time_elapsed + 1
                time.sleep(1)
            else:
                tcp_connection_closed = True
                break

        if not tcp_connection_closed:
            raise Exception("TCP Connections not closed in 1 min")

        checkpoint_command = f"podman container checkpoint --file-locks -e /dumps/check.tar.gz {new_container.id}"

        checkpoint_output = getstatusoutput(checkpoint_command)
        checkpoint_output_text = checkpoint_output[1]
        if checkpoint_output[0] != 0:
            raise Exception(
                f"Could not dump container {new_container}: {checkpoint_output_text}"
            )
        else:
            client.containers.remove(new_container)
    else:
        print(dump_results[1].decode("utf8", "ignore"))

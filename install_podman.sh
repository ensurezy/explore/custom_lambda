topdir=${HOME}/work
mkdir -p ${topdir}
mkdir ${topdir}/Downloads

cd ${topdir}/Downloads
git clone https://github.com/openSUSE/catatonit.git
cd catatonit
./autogen.sh
./configure
make
make install

cd ${topdir}/Downloads
git clone https://github.com/opencontainers/runc.git
cd runc
make
make install

# Install containers-common
cd ${topdir}/Downloads
git clone https://github.com/containers/conmon
cd conmon
make
make podman

# Build libslirp
echo "=> Building libslirp..."
cd ${topdir}/Downloads
git clone https://gitlab.freedesktop.org/slirp/libslirp.git
cd libslirp
git switch stable-4.2
meson build
ninja -C build
ninja -C build install

# Build slirp4netns
echo "=> Building slirp4netns..."
cd ${topdir}/Downloads
git clone https://github.com/rootless-containers/slirp4netns.git
cd slirp4netns
git switch release/0.4
./autogen.sh
./configure --prefix=/usr/local
make -j
make install

# Build podman
echo "=> Building podman..."
cd ${topdir}/Downloads
git clone https://github.com/containers/podman
cd podman
git switch --detach v5.1.1
make BUILDTAGS="exclude_graphdriver_btrfs exclude_graphdriver_devicemapper seccomp selinux systemd"
make install

FROM docker.io/amazonlinux:2023

COPY filesystem/criu/ filesystem/podman/ /root/work/Downloads/

RUN yum install -y yajl-devel python-protobuf protobuf-c libbsd-devel libnet-devel systemd-devel cmake iptables \
    libseccomp-devel rpm-build libselinux-devel protobuf asciidoc cni-plugins unzip gpgme-devel golang nftables \
    golang-github-cpuguy83-md2man.x86_64 nftables-devel libtool xmlto procps-ng yajl libnl3-devel libcap-devel \
    pkgconfig git patch gcc-c++ gnutls-devel vim gcc iproute libdrm-devel make protobuf-compiler cargo \
    protobuf-c-devel meson iptables-nft automake autoconf protobuf-devel \
    /root/work/Downloads/aardvark-dns-1.9.0-1.el9.x86_64.rpm /root/work/Downloads/netavark-1.11.0-1.el9.x86_64.rpm && \
    python3 -m ensurepip

RUN cd /root/work/Downloads && \
    rm aardvark-dns-1.9.0-1.el9.x86_64.rpm && rm netavark-1.11.0-1.el9.x86_64.rpm && \
    unzip -x criu.zip && rm criu.zip && \
    unzip -x catatonit.zip && rm catatonit.zip && \
    unzip -x runc.zip && rm runc.zip && \
    unzip -x conmon.zip && rm conmon.zip && \
    unzip -x libslirp.zip && rm libslirp.zip && \
    unzip -x slirp4netns.zip && rm slirp4netns.zip && \
    unzip -x podman.zip && rm podman.zip && \
    cd /root/work/Downloads/criu && make install && cd lib && pip3 install . && \
    cd /root/work/Downloads/catatonit && make install && \
    cd /root/work/Downloads/runc && make install && \
    cd /root/work/Downloads/conmon && make podman && \
    cd /root/work/Downloads/libslirp && ninja -C build install && \
    cd /root/work/Downloads/slirp4netns && make install && \
    cd /root/work/Downloads/podman && make install && \
    cd /root/work/Downloads && \
    rm -rf criu && rm -rf catatonit && rm -rf runc && rm -rf conmon && rm -rf libslirp && rm -rf slirp4netns && \
    rm -rf podman

COPY container_policy.json /etc/containers/policy.json
COPY criu.conf /etc/criu/runc.conf

COPY filesystem/x11vnc/ filesystem/ffmpeg/ filesystem/chrome/ /filesystem/

COPY requirements.txt /requirements.txt
RUN pip3 install -r requirements.txt

COPY src/requirements.txt src/requirements.txt

VOLUME /var/lib/containers

COPY docker_entrypoint.sh /docker_entrypoint.sh

ENTRYPOINT ["/bin/bash", "/docker_entrypoint.sh"]